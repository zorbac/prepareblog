'''
Created on 6 mai 2014

@author: jingl3s

Copyright 2014 jingl3s 
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD license (see the file
COPYING.txt included with the distribution).
'''

import configparser
import logging
import os
from configparser import ConfigParser
from os.path import dirname


class Section():
    SectionName="PrepBlog"
#     PrepBlog=socket.gethostname() + "-" + sys.platform

class Fields():
    # r"/home/soniahugo/Images/Photo/2014_Canada2/Blog"
    SRC_FOLDER="SRC_FOLDER"
    
    BLOG_LOGIN="BLOG_LOGIN"
    BLOG_PASS="BLOG_PASS"
    
    # "http://jingl3s.blog.free.fr/admin"
    BLOG_URL_ADMIN="BLOG_URL_ADMIN"
    
    # Quebec_2014/Montreal2/
    BLOG_URL_MEDIA_FOLDER_USED="BLOG_URL_MEDIA_FOLDER_USED"
    
    # Media folder used with the complement elements of BLOG_URL_MEDIA_FOLDER_USED
    # BLOG_URL_MEDIA_FOR_LINK + BLOG_URL_MEDIA_FOLDER_USED
    BLOG_URL_MEDIA_FOR_LINK="BLOG_URL_MEDIA_FOR_LINK"
    
    # Category used in drop down list
    # "Canada_2014"
    BLOG_POST_CATEGORY = "BLOG_POST_CATEGORY" 
    
    # Default fixed title used
    # "Vie a Montreal S2" 
    BLOG_POST_TITLE = "BLOG_POST_TITLE" 
   

class GestionPrefs(object):
    '''
    classdocs
    '''
    
    #===========================================================================
    # 
    #===========================================================================
    def __init__(self, path, filename):
        self._prefs = {}
        self._config = configparser.RawConfigParser()
        self._section_name = Section.SectionName
        self._filename = path + os.path.sep + os.path.splitext(os.path.split(filename)[1])[0] + ".ini"
        logging.basicConfig()
        self._logger = logging.getLogger(__file__)

    #===========================================================================
    # 
    #===========================================================================
    def getConfigFilename(self):
        return self._filename 

    #===========================================================================
    # 
    #===========================================================================
    def _createDefaultSection(self, _config_filename):
        self._config.add_section(self._section_name)
        
        members = [attr for attr in dir(Fields()) if not callable(attr) and not attr.startswith("__")]
        
        for field in members:
            self._config.set(self._section_name, field, '')
        
        # Writing configuration file
        with open(_config_filename, 'wb') as configfile:
            self._config.write(configfile)
            configfile.close()
        
        os.system(_config_filename)
        
        self._logger.info( "-----------------------------------------------\n" + \
        "Please configure file before processing.\n" + \
        str(_config_filename) + "\n" + \
        "-----------------------------------------------")

    #===========================================================================
    # 
    #===========================================================================
    def get_preferences(self):
        # Creation of default empty config file if not present
        if os.path.exists(self._filename) == False:
            self._createDefaultSection(self._filename)
            return None
        
        # Get the different elements in configuration
        self._config.read(self._filename)
        
        if self._config.has_section(self._section_name) == False:
            self._createDefaultSection(self._filename)
            return None
        
        members = [attr for attr in dir(Fields()) if not callable(attr) and not attr.startswith("__")]
        _preference = {}
        for field in members:
            _preference[field] = self._config.get(self._section_name, field.lower())
            if _preference[field] == "":
                self._logger.info("-----------------------------------------------"\
                + "The following field is empty please fill all fields." \
                + "File: " + self._filename \
                + "Field: " + field.lower() \
                + "-----------------------------------------------")
                return None
        
        return _preference
    
    #===========================================================================
    # 
    #===========================================================================
    def write_preference(self, p_data, p_field):
        
        # Get the different elements in configuration
        self._config.read(self._filename)
        
        if self._config.has_section(self._section_name) == False:
            self._createDefaultSection(self._filename)
            return False
        
        self._config.set(self._section_name, p_field, p_data)
        
        with open(self._filename, 'wb') as configfile:
            self._config.write(configfile)
            configfile.close()
        
if __name__ == "__main__":
    
    path = dirname(__file__) + os.path.sep + ".." + os.path.sep
    filename_python = "PrepareBlog.py"
    pref = GestionPrefs(path, filename_python)
    prefs = pref.get_preferences()
    print (prefs)
    pref.write_preference(prefs[Fields.BLOG_POST_TITLE], Fields.BLOG_POST_TITLE)

