# -*- coding: latin-1 -*-
'''
Created on 9 nov. 2014

@author: jingl3s

Copyright 2014 jingl3s 
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD license (see the file
COPYING.txt included with the distribution).
'''

import mechanize
from mechanize._opener import urlopen
#from mechanize._form import ParseResponse
import string
import os
import re
import logging
from mechanize._beautifulsoup import BeautifulSoup


class InterractionBlog():

    '''
    InterractionBlog: Classe pour acc�der au blog dotclear de free.fr
    '''
    PROP_TAG_USER = "user_id"
    PROP_TAG_PWD = "user_pwd"
    PROP_TAG_FILE_TITLE = "upfiletitle"
    PROP_TAG_FILE_UP = "upfile"
    PROP_TAG_POST_TITLE = "post_title"
    PROP_TAG_POST_CONTENT = "post_content"
    PROP_TAG_POST_STATUS = "post_status"
    PROP_TAG_POST_CATEGORY = "cat_id"

    def __init__(self, p_blog_address, p_media_folder, p_blog_category, p_test_mode=False):
        '''
        Constructor
        '''
        self._form = None
        self._blog_address = p_blog_address

        self.PROP_URL_MEDIA = self._blog_address\
            + "/media.php?popup=0&post_id=&d="\
            + p_media_folder
        self.PROP_URL_POST = self._blog_address\
            + "/post.php"

        self._blog_post_category = p_blog_category

        logging.basicConfig()
        self._logger = logging.getLogger(__file__)

        self._test_mode = p_test_mode

    #=========================================================================
    # Login to the blog with address given display the login page
    #=========================================================================
    def login_blog(self, p_login, p_pass):

        self._logger.info("Blog login in progress")

        if not self._test_mode:
            # Create a web request with the url
            request = mechanize.Request(self._blog_address)
            # Open the prepared request
            response = mechanize.urlopen(request)
            # Get the forms inside the web page
            forms = mechanize.ParseResponse(response, backwards_compat=False)

            # I don't want to close?!
            # response.close()

            # Username and Password are stored in the form containing the
            # control name
            form = self.__get_form_from_control_name(forms, self.PROP_TAG_USER)

            form[self.PROP_TAG_USER] = p_login
            form[self.PROP_TAG_PWD] = p_pass

            # Display the content of entered text
            if self._logger.isEnabledFor(logging.DEBUG):
                self._logger.debug(form[self.PROP_TAG_USER])
                self._logger.debug(form[self.PROP_TAG_PWD])

        if not self._test_mode:

            # Execute the form validation, to login into the admin page
            # Did not get the response because we are going to another page
            response = urlopen(form.click())

            # Check login status return
            data = response.read()
        else:
            self._logger.info("Test mode actif, aucune connexion au serveur")
            data = "Tableau de bord"

        if string.find(data, "Tableau de bord") == -1:
            self._logger.error("Blog login failed")
            return False
        else:
            self._logger.info("Blog login completed")
            return True

    #=========================================================================
    # Send the Picture to the blog
    #=========================================================================
    def upload_photo(self, filename):
        '''
        Upload a photo to the blog after a successfull login

        @param 
        filename: the filename to upload

        '''

        filename_only = os.path.basename(filename)
        self._logger.info("Blog uploading: " + filename_only)

        if not self._test_mode:

            # Open the url if the response is not set
            request = mechanize.Request(self.PROP_URL_MEDIA)
            response = mechanize.urlopen(request)

            # Get the forms inside the page
            forms = mechanize.ParseResponse(response, backwards_compat=False)

            # Get the corresponding form
            form_in_progress = self.__get_form_from_control_name(
                forms, self.PROP_TAG_FILE_TITLE)

            # Equivalent a : form_in_progress[self.PROP_TAG_FILE_TITLE] =
            # filename_only 
            form_in_progress = self.__get_form_from_control_name(
                forms, self.PROP_TAG_FILE_TITLE)

            control = form_in_progress.find_control(self.PROP_TAG_FILE_TITLE)
            control.value = filename_only

            self._logger.debug(
                "self.PROP_TAG_FILE_TITLE: %s" % form_in_progress[self.PROP_TAG_FILE_TITLE])

            control = form_in_progress.find_control(self.PROP_TAG_FILE_UP)
            control.add_file(open(filename), 'image/jpeg', filename)
            self._logger.debug("form_in_progress[self.PROP_TAG_FILE_UP]: %s" % form_in_progress[
                               self.PROP_TAG_FILE_UP])

            # Validate the formula to upload the file
            request = form_in_progress.click()
            response = mechanize.urlopen(request)

            # Read the return to check if the picture is correctly uploaded
            data = response.read()
        else:
            self._logger.info("Test mode actif, aucun envoi de fichier")
            data = filename_only

        # La commande suivante ne marche pas avec la reponse obtenu
        # response.seek(0)

        from bs4 import BeautifulSoup

        soup = BeautifulSoup(data)

#         list_paraph = soup.find_aall('p')
        b_trouve = False
        for paraph in soup.find_all('p'):
            #     for paraph in soup.p:
            if "Fichier charg".lower() in paraph.string.lower() and "avec succ".lower() in paraph.string.lower():
                b_trouve = True
                break

# if string.find(data, "\"message\">Fichier charg� avec succ�s.") == -1:
        if not b_trouve:
            #         if string.find(data, filename_only) == -1:
            self._logger.error(
                "Blog image load fails: " + filename_only + ".\nDonn�e recu : " + str(data))

    #=========================================================================
    # Create a new ticket to the blog
    #=========================================================================
    def create_new_ticket(self, p_tuple_text, picture_name_to_check, p_post_title):
        '''
        Create a new ticket on the blog

        @param 
        filename: the filename to upload

        '''

        if self._test_mode:
            return ""

        # Open the url if the response is not set
        request = mechanize.Request(self.PROP_URL_POST)
        response = mechanize.urlopen(request)

        # Get the forms inside the page
        forms = mechanize.ParseResponse(response, backwards_compat=False)

        # Get the corresponding form
        form_in_progress = self.__get_form_from_control_name(
            forms, self.PROP_TAG_POST_TITLE)

        # Put the title
        control = form_in_progress.find_control(self.PROP_TAG_POST_TITLE)
        control.value = p_post_title
        self._logger.debug(form_in_progress[self.PROP_TAG_POST_TITLE])

        # Put the content
        control = form_in_progress.find_control(self.PROP_TAG_POST_CONTENT)
        content = '\n'.join(map(str, p_tuple_text))
        control.value = content
        self._logger.debug(form_in_progress[self.PROP_TAG_POST_CONTENT])

        # Set the status to not published
        control = form_in_progress.find_control(self.PROP_TAG_POST_STATUS)
        control.value = ['0', ]
        self._logger.debug(form_in_progress[self.PROP_TAG_POST_STATUS])

        # Set the Category (Canada_2014)
        control = form_in_progress.find_control(self.PROP_TAG_POST_CATEGORY)
        for _item in control.items:
            if string.find(_item._labels[0]._text, self._blog_post_category) != -1:
                control.value = [_item.name, ]
                self._logger.debug(
                    form_in_progress[self.PROP_TAG_POST_CATEGORY])
                break

        # Validate the formula to upload the file
        request = form_in_progress.click()
        response = mechanize.urlopen(request)

        # Read the return to check if the picture is correctly uploaded
        data = response.read()

        pattern = r"post\.php\?id=(\d+)&amp"

        addresse = ""
        if string.find(data, picture_name_to_check) == -1:
            self._logger.error("Blog create new ticket fails.")
        else:
            self._logger.info("Blog create new ticket success.")
            m = re.search(pattern,
                          data)
            if m:
                addresse = self.PROP_URL_POST + \
                    "?id=" + m.groups()[0] + "&crea=1"

        return addresse

    #=========================================================================
    # create the corresponding line expected to be used by the blog ticket
    #=========================================================================
    @staticmethod
    def get_blog_line(blog_path, image_name, thumb_img=None):

        filename_tuple = os.path.splitext(image_name)
        string_return = "[(("
        # Ajout de ma propre miniature
        if thumb_img is None:
            string_return += blog_path + "." + \
                filename_tuple[0] + "_m" + filename_tuple[1].lower() + "|"
        else:
            string_return += blog_path + thumb_img + "|"

        string_return += " |"
        string_return += "C|"
        string_return += filename_tuple[0] + "))|"
        string_return += blog_path + image_name + "]"
        string_return += "%%%"
        return string_return

    #=========================================================================
    # From a list of form search the corresponding control
    #=========================================================================
    def __get_form_from_control_name(self, p_forms, p_str_control_name):
        '''
        Search form containing the title edit label

        @param 
        p_forms: list of HTMLForm
        p_str_control_name: string of the control name to find

        '''
        #
        for form in p_forms:
            try:
                if form.find_control(p_str_control_name) != None:
                    break
            except mechanize.ControlNotFoundError:
                continue
        return form
